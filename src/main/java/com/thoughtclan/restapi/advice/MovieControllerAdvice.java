package com.thoughtclan.restapi.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.thoughtclan.restapi.exception.MovieNotFoundException;

@RestControllerAdvice
public class MovieControllerAdvice {
	
	@ExceptionHandler(MovieNotFoundException.class)
	@ResponseStatus(value= HttpStatus.NOT_FOUND)
	public String handleNotFoundException(MovieNotFoundException ex) {
        return "No Movie found based upon given Movie name or Movie id.";
    }
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(value= HttpStatus.BAD_REQUEST)
	public String handleBadRequestException(MethodArgumentNotValidException ex) {
        return "Input not valid." + ex.getParameter();
    }

	/**
	 * TODO: Could use a common error model in case of REST application, instead of returning String. Something like an "ErrorData" model.
	 */
	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseStatus(value= HttpStatus.BAD_REQUEST)
	public String handleIllegalArgumentException(IllegalArgumentException ex) {
        return "Input not  valid." + ex.getMessage();
    }

}
