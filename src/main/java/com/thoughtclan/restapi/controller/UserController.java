package com.thoughtclan.restapi.controller;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.thoughtclan.restapi.dto.MovieDto;
import com.thoughtclan.restapi.dto.UserDto;
import com.thoughtclan.restapi.model.Movie;
import com.thoughtclan.restapi.model.User;
import com.thoughtclan.restapi.repository.UserRepository;
import com.thoughtclan.restapi.service.Impl.UserServiceImpl;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserRepository userRepo;
	
	
	  @Autowired 
	  private UserServiceImpl userService;
	 
	@PostMapping("/adduser")
	public User addUser(@RequestBody UserDto userDto)
	{
		User user=new User();
		user.setName(userDto.getName());
		user.setPassword(userDto.getPassword());
		user.setEmail(userDto.getEmail());
		userRepo.save(user);
		return user;
	}
	
	
	  @GetMapping("/getalluser") 
	  public List<User> getAllUser(){
	  
	        return userService.getAllUser();
	  
	  }
	  
	  @PatchMapping("/{userId}/addmovie")
	  public User addMovie(@RequestBody MovieDto movieDto,@PathVariable int userId)
	  {
		  return userService.addMovie(movieDto, userId);
	  }
	  
	  @DeleteMapping("/{userId}/remove/{filmId}")
	  public User removeMovie(@PathVariable int userId,@PathVariable Long filmId )
	  {
		  return userService.removeMovie(userId, filmId);
	  }
	  
	  @GetMapping("/{userId}/allWatchlist")
	  public Collection<Movie> allWatchlist(@PathVariable int userId)
	  {
		  return userService.allWatchlist(userId);
	  }
	 
}
