package com.thoughtclan.restapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.thoughtclan.restapi.dto.MovieDto;
import com.thoughtclan.restapi.model.Movie;
import com.thoughtclan.restapi.repository.MovieRepository;
import com.thoughtclan.restapi.service.Impl.MovieServiceImpl;

@RestController
@RequestMapping("/movie")
public class MovieController {
	
	@Autowired
	private MovieRepository movieRepo;
	
	
	  @Autowired
	  private MovieServiceImpl movieService;
	 
	@PostMapping("/addmovie")
	public Movie addMovie(@RequestBody MovieDto movieDto) {
		Movie movie=new Movie();
		movie.setMovieName(movieDto.getMovieName());
		movie.setStudio(movieDto.getStudio());
		movie.setRating(movieDto.getRating());
		movie.setReleaseYear(movieDto.getReleaseYear());
		movieRepo.save(movie);
		return movie;
	}
	
	  @GetMapping("/rating")
	  public List<Movie> getMovieOnRating(@RequestBody MovieDto movieDto)
	  {
		 
	     String rating=movieDto.getRating();
	    return movieService.getAllMovie(rating);
	  
	  }
	  
	  @GetMapping("/moviename")
	  public Movie getMovieByName(@RequestBody MovieDto movieDto) {
		  String name=movieDto.getMovieName();
			return movieService.getMovieByName(name);
		}
		
	 
}
