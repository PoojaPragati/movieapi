package com.thoughtclan.restapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.thoughtclan.restapi.model.Movie;

@Repository
public interface MovieRepository extends PagingAndSortingRepository<Movie, Long>, JpaSpecificationExecutor<Movie>{

	List<Movie> findByRatingOrderByReleaseYearAsc(String rating);
	
	Movie findByMovieName(String movieName);
	Movie findByFilmId(Long id);
}
