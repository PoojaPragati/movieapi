package com.thoughtclan.restapi.dto;

public class AddMovieDto {
	public String movieName;
	public int userId;
	
	public AddMovieDto() {
	
		// TODO Auto-generated constructor stub
	}

	public AddMovieDto(String movieName) {
		super();
		this.movieName = movieName;
	}

	public String getMovieName() {
		return movieName;
	}

	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "AddMovieDto [movieName=" + movieName + ", userId=" + userId + "]";
	}

	

}
