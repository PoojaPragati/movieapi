package com.thoughtclan.restapi.dto;

import java.time.Year;

public class MovieDto {
	
	public Long filmId;
	public String movieName;
	public String rating;
	public String studio;
	public Year releaseYear;
	public Integer addMovie;
	
	public MovieDto() {
		// TODO Auto-generated constructor stub
	}

	public Long getFilmId() {
		return filmId;
	}

	public void setFilmId(Long filmId) {
		this.filmId = filmId;
	}

	public String getMovieName() {
		return movieName;
	}

	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}


	public String getStudio() {
		return studio;
	}

	public void setStudio(String studio) {
		this.studio = studio;
	}

	public Year getReleaseYear() {
		return releaseYear;
	}

	public void setReleaseYear(Year releaseYear) {
		this.releaseYear = releaseYear;
	}
	

	public Integer getAddMovie() {
		return addMovie;
	}

	public void setAddMovie(Integer addMovie) {
		this.addMovie = addMovie;
	}

	@Override
	public String toString() {
		return "MovieDto [filmId=" + filmId + ", movieName=" + movieName + ", rating=" + rating + ", studio=" + studio
				+ ", releaseYear=" + releaseYear + ", addMovie=" + addMovie + "]";
	}

	public MovieDto(Long filmId, String movieName, String rating, String studio, Year releaseYear, Integer addMovie) {
		super();
		this.filmId = filmId;
		this.movieName = movieName;
		this.rating = rating;
		this.studio = studio;
		this.releaseYear = releaseYear;
		this.addMovie = addMovie;
	}

	
	
	

}
