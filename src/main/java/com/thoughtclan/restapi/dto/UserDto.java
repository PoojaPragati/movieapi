package com.thoughtclan.restapi.dto;

import java.util.Collection;


public class UserDto {
	
	public int id;
	public String name;
	public String password;
	public String email;
	public Collection<MovieDto> movie;
	
	public UserDto() {
		
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Collection<MovieDto> getMovie() {
		return movie;
	}

	public void setMovie(Collection<MovieDto> movie) {
		this.movie = movie;
	}

	@Override
	public String toString() {
		return "UserDto [id=" + id + ", name=" + name + ", email=" + email + ", movie=" + movie + "]";
	}

	public UserDto(int id, String name, String password, String email, Collection<MovieDto> movie) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
		this.email = email;
		this.movie = movie;
	}
	
	
	
	

}
