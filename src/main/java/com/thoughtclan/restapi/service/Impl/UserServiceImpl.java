package com.thoughtclan.restapi.service.Impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.thoughtclan.restapi.dto.MovieDto;
import com.thoughtclan.restapi.dto.UserDto;
import com.thoughtclan.restapi.model.Movie;
import com.thoughtclan.restapi.model.User;
import com.thoughtclan.restapi.repository.MovieRepository;
import com.thoughtclan.restapi.repository.UserRepository;
import com.thoughtclan.restapi.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private MovieRepository movieRepo;

	@Override
	public List<User> getAllUser() {
		
		return userRepo.findAll();
	}
	
	public User addUser(@RequestBody UserDto userDto)
	{
		User user=new User();
		user.setName(userDto.getName());
		user.setPassword(userDto.getPassword());
		user.setEmail(userDto.getEmail());
		userRepo.save(user);
		return user;
	}
	
	public User addMovie( MovieDto movieDto, int userId)
	{
		ArrayList<Movie> a1=new ArrayList<Movie>();
		Movie movie=movieRepo.findByMovieName(movieDto.getMovieName());
		a1.add(movie);
		User user=userRepo.findById(userId);
		for(int i=0;i<a1.size();i++)
			user.setMovie(a1);
		
		  int addmovie = movie.getAddMovie() != null ?
		  movie.getAddMovie() + 1 : 1;
		  movie.setAddMovie(addmovie);
		  movieRepo.save(movie);
		  userRepo.save(user);
		  return user;
	}
	
	public User removeMovie(int userId, Long filmId)
	{
		User user=userRepo.findById(userId);
		Collection<Movie> movies=user.getMovie();
		Movie movie=movieRepo.findByFilmId(filmId);
		if(movies.contains(movie))
			movies.remove(movie);
		user.setMovie(movies);
		userRepo.save(user);
		return user;
	}
	
	public Collection<Movie> allWatchlist(int userId)
	{
		User user=userRepo.findById(userId);
		Collection<Movie> movie=user.getMovie();
		return movie;
	}

}
