package com.thoughtclan.restapi.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thoughtclan.restapi.model.Movie;
import com.thoughtclan.restapi.repository.MovieRepository;
import com.thoughtclan.restapi.service.MovieService;

@Service
public class MovieServiceImpl implements MovieService {
	
	@Autowired
	private MovieRepository movieRepo;

	@Override
	public List<Movie> getAllMovie(String rating) {
		
		return movieRepo.findByRatingOrderByReleaseYearAsc(rating);
	}
	
	public Movie getMovieByName(String name) {
		return movieRepo.findByMovieName(name);
	}
	
	

}
