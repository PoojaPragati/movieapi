package com.thoughtclan.restapi.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.thoughtclan.restapi.dto.MovieDto;
import com.thoughtclan.restapi.model.Movie;
import com.thoughtclan.restapi.model.User;

public interface UserService {
	
	public List<User> getAllUser();
	public User addMovie( MovieDto movieDto, int userId);
	public User removeMovie(int userId, Long filmId);
	public Collection<Movie> allWatchlist(int userId);
	
	
}
