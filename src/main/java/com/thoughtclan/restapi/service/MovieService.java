package com.thoughtclan.restapi.service;

import java.util.List;

import com.thoughtclan.restapi.model.Movie;

public interface MovieService {
	
	public List<Movie> getAllMovie(String rating);
	public Movie getMovieByName(String name);

}
