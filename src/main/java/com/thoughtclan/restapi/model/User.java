package com.thoughtclan.restapi.model;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

	/**
	 * JPA entity class for persisting Users.
	 */
@Entity
public class User {

		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)
		public int id;
		
		@Column
		public String name;
		
		@Column
		public String password;
		
		@Column
		public String email;
		
		@ManyToMany(fetch=FetchType.LAZY)
		public Collection<Movie> movie;
		
		
		
	
		public User() {
			
			// TODO Auto-generated constructor stub
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public Collection<Movie> getMovie() {
			return movie;
		}

		public void setMovie(Collection<Movie> movie) {
			this.movie = movie;
		}

		public User(int id, String name, String password, String email, Collection<Movie> movie) {
			super();
			this.id = id;
			this.name = name;
			this.password = password;
			this.email = email;
			this.movie = movie;
		}

		
	


}
