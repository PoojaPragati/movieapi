package com.thoughtclan.restapi.model;

import java.time.Year;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Movie {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long filmId;
	
	@Column
	public String movieName;
	
	@Column
	public String rating;
	
	@Column
	public String studio;
	
	@Column
	public Year releaseYear;
	
	@Column
	public Integer addMovie;
	
	

	public Movie() {
		
		// TODO Auto-generated constructor stub
	}

	public Long getFilmId() {
		return filmId;
	}

	public void setFilmId(Long filmId) {
		this.filmId = filmId;
	}

	public String getMovieName() {
		return movieName;
	}

	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	

	public String getStudio() {
		return studio;
	}

	public void setStudio(String studio) {
		this.studio = studio;
	}

	public Year getReleaseYear() {
		return releaseYear;
	}

	public void setReleaseYear(Year releaseYear) {
		this.releaseYear = releaseYear;
	}

	public Integer getAddMovie() {
		return addMovie;
	}

	public void setAddMovie(Integer addMovie) {
		this.addMovie = addMovie;
	}

	public Movie(Long filmId, String movieName, String rating, String studio, Year releaseYear, Integer addMovie) {
		super();
		this.filmId = filmId;
		this.movieName = movieName;
		this.rating = rating;
		this.studio = studio;
		this.releaseYear = releaseYear;
		this.addMovie = addMovie;
	}
	
	
	

}
